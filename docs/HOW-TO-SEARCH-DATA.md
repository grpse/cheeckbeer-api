# How to search via api

### Via URL parameters
    - GET /api/<model>/?filter[where][property][regexp]=<expression>
    - GET /api/<model>/?filter[where][property][regexp]=<expression>&filter[limit]=<number>

    - Exemple where name starts with "Sk":
        - GET /api/products/?filter[where][name][regexp]="^Sk"