# CRUD for this API

### Create:
    - POST /api/<model>/
    - Body with data described for the model
    - Header: Authorization : ACCESS_TOKEN or <full url>?ACCESS_TOKEN=<access token received from login>

### Read (no authentication required): 
    - GET /api/<model>/ or individual GET /api/<model>/<model id>

### Update:
    - PUT /api/<model>/<model id>
    - Body with data described for the model
    - Header: Authorization : ACCESS_TOKEN or <full url>?ACCESS_TOKEN=<access token received from login>

### Delete:
    - DELETE /api/<model>/<model id>
    - Header: Authorization : ACCESS_TOKEN or <full url>?ACCESS_TOKEN=<access token received from login>

## Notes:
    - Some models have special methods and different relations, checkout documentation to get the full specification of relations.