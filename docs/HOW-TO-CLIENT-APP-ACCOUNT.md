# How to create an account normal and via facebook

### Normal way:

    - [POST /api/client/register] with all required data in request body 
    - [POST /api/client/login] with email and password. Will return the token needed cross app 

### Facebook way:
#### Note: App must have permission to get name, email and birthday of the user

    - Send a facebook field as an object containing 4 fields {id, name, email, birthday}
      to [POST /api/client/register].
    - Login, send an object containing 2 fields {email, password} (password will be facebook id)
      to [POST /api/client/login]. Will return a token if success.
