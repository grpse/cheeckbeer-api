# How to Admin 
## How to create an admin account
---
### Temporary:

- ### Via API:
    - Send a POST request to /api/admin/create with email and password

### Production:

- ### Via Script:
    - Add a new entry at server/admins.json within array that should contain {email,password,name}
    - Start the server

## How to login

- ### Login
    - Send via api email and password /api/admin/login