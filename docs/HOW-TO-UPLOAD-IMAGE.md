# HOW TO UPLOAD A IMAGE

## Follow this steps
    - Post an image multipart to <server url>/file/upload
    - If property "fileid" returns with a value, then this is the file name to access
    - Save this "fileid" as property "imageId" of the model that you are handling
    - Image access can be achieved throught "/file/<model.imageId>"