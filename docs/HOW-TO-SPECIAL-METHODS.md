# Special methods in depth

## Products Highlights [GET /api/products/highlights]
    
    Returns the best price match by price/ml

## Near brands/brandlines/products/retailers [GET /api/products/near]

    - Just need to provide lat (latiture), lng (longiture) and radius to search all near by. 


## Bookmark:

    - [GET /api/client/:id/bookmarks/] returns all bookmarks
    - [GET /api/client/:id/bookmarks/:fk] get a specific bookmark
    - [POST /api/client/:id/bookmarks/] make a bookmark, just need to send productId or barId and type [product|bar]
    - [PUT /api/client/:id/bookmarks/:fk] update a specific bookmark
    - [DELETE /api/client/:id/bookmarks/:fk] delete bookmark by id
    