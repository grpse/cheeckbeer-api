# How to get products with price in a collectors account
## Steps
- Send a get request to '/api/Products/productsWithPrice'
    - it will return a list of product x retailer relation with product, brand line, brand, retailer, price and availabe
- To make some pagination of the search send a request with 'page' and 'limit' parameters to the same URL:
    - Ex.: '/api/Products/productsWithPrice?page=2&limit=10'. Page 2 and limit 10 per page.