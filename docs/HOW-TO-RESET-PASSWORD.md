# How to reset password for client

### Steps:
    1. Send a {email : <email of the account>} POST /requestPasswordReset
    2. Receive returned message:
        2.1. 200, application/json {message : 'Password reset was successfull. Check out your email.'} if successfull
        2.2. 401, application/json {error: <informative error>} if NOT successfull
    3. Check email with link to recover password:
        3.1. Link is to open a page (URL, but could open a app page)
    4. Send a {password: <new password>, confirmation: <new password again>} POST /resetPassword
        4.1 If successfull, 200, application/json, { message: 'Password reset success. Your password has been reset successfully'}
