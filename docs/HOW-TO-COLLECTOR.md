# Hot to Collector
## How to create a Collector account
---
### Temporary:

- ### Via API:
    - Send a POST request to /api/collector/create with email and password

### Production:

- ### Via Script:
    - Add a new entry at server/collectors.json within array that should contain {email,password,name}
    - Start the server

## How to login

- ### Login
    - Send via api email and password /api/collector/login