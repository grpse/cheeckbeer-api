# How to change product price and availability

- Login with Collector [See HOW-TO-COLLECTOR.md]
- Send a request to /api/products/<id>/retailers/<fk> with relation data updated {available: <true or false>, price : <any integer number(decimals should be calculated dividing by 100)>}