# How to read API-DOC

- #### Option 1 (Easy):
    - Read API-DOC.pdf 

- #### Option 2:
    - Read API-DOC.html

- #### Option 3 (Hard):
    - Open swagger.io
    - Find online editor demo
    - Paste API-DOC.yaml into editable area
    - Read generated formated documentation
    - Optional: Make requests with online editor