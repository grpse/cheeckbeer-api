module.exports = {
  "restApiRoot": "/api",
  "host": "localhost",
  "port": 3000,
  "remoting": {
    "context": false,
    "rest": {
      "normalizeHttpPath": false,
      "xml": false
    },
    "json": {
      "strict": false,
      "limit": "1gb"
    },
    "urlencoded": {
      "extended": true,
      "limit": "1gb"
    },
    "cors": false,
    "handleErrors": false
  },
  "legacyExplorer": false
}