module.exports = {
  "restApiRoot": "/api",
  "host": process.env.CUSTOM_HOST || "0.0.0.0",
  "port": process.env.CUSTOM_PORT || 3000,
  "remoting": {
    "context": false,
    "rest": {
      "normalizeHttpPath": false,
      "xml": false
    },
    "json": {
      "strict": false,
      "limit": "1gb"
    },
    "urlencoded": {
      "extended": true,
      "limit": "1gb"
    },
    "cors": true,
    "handleErrors": false
  },
  "legacyExplorer": false
}