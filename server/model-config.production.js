console.log('model-config:', process.env.NODE_ENV);
module.exports = {
  "_meta": {
    "sources": [
      "loopback/common/models",
      "loopback/server/models",
      "../common/models",
      "./models"
    ],
    "mixins": [
      "loopback/common/mixins",
      "loopback/server/mixins",
      "../common/mixins",
      "./mixins"
    ]
  },
  "Admin": {
    "dataSource": "mongodb_local",
    "public": true,
    "$promise": {},
    "$resolved": true
  },
  "Collector": {
    "dataSource": "mongodb_local",
    "public": true,
    "$promise": {},
    "$resolved": true
  },
  "Client": {
    "dataSource": "mongodb_local",
    "public": true,
    "$promise": {},
    "$resolved": true
  },
  "AccessToken": {
    "dataSource": "mongodb_local",
    "public": false
  },
  "ACL": {
    "dataSource": "mongodb_local",
    "public": false
  },
  "RoleMapping": {
    "dataSource": "mongodb_local",
    "public": false
  },
  "Role": {
    "dataSource": "mongodb_local",
    "public": false
  },
  "Brand": {
    "dataSource": "mongodb_local",
    "public": true,
    "$promise": {},
    "$resolved": true
  },
  "BrandLine": {
    "dataSource": "mongodb_local",
    "public": true,
    "$promise": {},
    "$resolved": true
  },
  "Product": {
    "dataSource": "mongodb_local",
    "public": true,
    "$promise": {},
    "$resolved": true
  },
  "Retailer": {
    "dataSource": "mongodb_local",
    "public": true,
    "$promise": {},
    "$resolved": true
  },
  "Bar": {
    "dataSource": "mongodb_local",
    "public": true,
    "$promise": {},
    "$resolved": true
  },
  "BarAd": {
    "dataSource": "mongodb_local",
    "public": true,
    "$promise": {},
    "$resolved": true
  },
  "BarImage": {
    "dataSource": "mongodb_local",
    "public": true,
    "$promise": {},
    "$resolved": true
  },
  "Banner": {
    "dataSource": "mongodb_local",
    "public": true,
    "$promise": {},
    "$resolved": true
  },
  "ProductRetailer": {
    "dataSource": "mongodb_local",
    "public": false,
    "$promise": {},
    "$resolved": true
  },
  "Bookmark": {
    "dataSource": "mongodb_local",
    "public": false
  },
  "CheckBeerMail": {
    "dataSource": "mailgun",
    "public": false
  }
}
