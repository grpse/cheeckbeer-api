//# /server/boot/autoupdate.js
"use strict";

let app = require('./server');
let ds = app.dataSources['mysql_dev'];
const async = require('async');



let lbTables = ['Application', 'Role', 'ACL', 'RoleMapping', 'AccessToken', 'Admin', 'Banner', 'Bar', 'BarAd', 'BarImage', 'Bookmark', 'Brand', 'BrandLine', 'Client', 'Collector', 'Product', 'ProductRetailer', 'Retailer'];

ds.automigrate(lbTables, function () {

    console.log(arguments);

});

// ds.isActual(lbTables, function (err, actual) {
//     if (!actual) {
//         ds.autoupdate(lbTables, function (err, result) {
//             //console.log(result);
//         });
//     }
// })