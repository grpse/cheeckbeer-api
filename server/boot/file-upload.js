'use strict';

/* Used to handle file uploader */
const multer = require('multer');
const bcrypt = require('bcryptjs');
const fs     = require('fs');
const path   = require('path');
const uploads= 'uploads/';


/* Object that handle files upload and creation */
const uploadDiskStorage = multer.diskStorage({
	destination: function(req, file, cb) {
        const exists = fs.existsSync(uploads);
        if (!exists) fs.mkdirSync(uploads);
		return cb(null, uploads);
	},
	filename: function(req, file, cb) {
		return bcrypt.hash(file.originalname + Date.now().toString(), 10, function(err, hashedName) {
			var fileNameStr;
			if (err != null) {
				return console.error(err);
			}
			fileNameStr = (hashedName + '.' + file.mimetype.split('/')[1]).replace(/[\/\\]/g, '');
			return cb(null, fileNameStr);
		});
	}
});


/* api argument to pre-process files to the right directory */
const uploader = multer({
	storage: uploadDiskStorage
});


module.exports = function(app){
    const router = app.loopback.Router();

    // Get a file if it exists
    router.get('/file/:id', function(req, res){
        // join final directory of the file
        const filePath = path.join(__dirname, '/../../', uploads, req.params.id);
        const exists = fs.existsSync(filePath);
        console.log(filePath, exists);
        if (exists) return res.sendFile(filePath);
        else return res.send('');
    });

    // Upload a new file to the system
    router.post('/file/upload', uploader.single('file'), function(req, res){
        if (req.file) return res.status(200).jsonp({fileid:req.file.filename});
        else return res.status(400).jsonp({fileid:''});
    });

    app.use(router);
}