"use strict";

const MongoClient = require('mongodb').MongoClient;
const mongoDBName = process.env.NODE_ENV.indexOf('development') >=0 ? 'CheckBeerDev' : 'CheckBeer';
const mongoURL = 'mongodb://localhost:27017/' + mongoDBName;
console.log("Database MongoURL: " + mongoURL);
console.log(process.env.NODE_ENV);
const async = require('async');

module.exports = function(app, andCallItWhenAllIndexationsAreDone) {

    MongoClient.connect(mongoURL, runEnsureIndexes);

    function runEnsureIndexes(connectError, db) {
        ensureIndexesOn(db, ['Retailer', 'Bar']);
    }

    function ensureIndexesOn(db, collectionsNames) {

        async.each(collectionsNames, ensureIndexOf, whenAllEnsuranceIsDone);

        function ensureIndexOf(collectionName, andCallItWhenIndexationDone) {
            let collection = db.collection(collectionName);
            collection.ensureIndex({ location: '2dsphere' }, function () {
                andCallItWhenIndexationDone();
            });
        }

        function whenAllEnsuranceIsDone(error) {
            andCallItWhenAllIndexationsAreDone();
        }
    }
}