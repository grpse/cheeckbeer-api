'use strict';

module.exports = function(app){

    const Client = app.models.Client;
    const router = app.loopback.Router();

    //send an email with instructions to reset an existing user's password
    router.post('/requestPasswordReset', function(req, res, next) {
        Client.resetPassword({email: req.body.email}, function(err) {
            
            if (err) return res.status(401).jsonp({error:err});

            res.status(200).jsonp({
                message : 'Password reset was successfull. Check out your email.'
            });
        });
    });

    //show password reset form
    router.get('/resetPassword', function(req, res, next) {
        
        if (!req.accessToken) return res.sendStatus(401);
        
        res.status(200).jsonp({
	        accessToken: req.accessToken.id
        });
    });

    //reset the user's pasword
    router.post('/resetPassword', function(req, res, next) {
        
        if (!req.accessToken) return res.sendStatus(401);

        //verify passwords match
        if (!req.body.password || !req.body.confirmation || req.body.password !== req.body.confirmation) {
            return res.sendStatus(400, new Error('Passwords do not match'));
        }

        Client.findById(req.accessToken.userId, function(err, client) {
            
            if (err) return res.sendStatus(404);

            client.updateAttribute('password', req.body.password, function(err, client) {
                
                if (err) return res.sendStatus(404);
                
                console.log('> password reset processed successfully');
                res.status(200).jsonp({
                    message: 'Password reset success. Your password has been reset successfully'
                });
            });
        });
    });

    app.use(router);
};