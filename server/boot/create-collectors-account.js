const fs = require('fs');
const async = require('async');

module.exports = function(app, callback){
    // get models needed
    const Collector = app.models.Collector;

    // get collectors server configuration
    const collectorsAccount = require('../collectors');
    
    if (!collectorsAccount.accounts) collectorsAccount.accounts = []; 
    
    async.filter(
        collectorsAccount.accounts, 
        createEachCollectorAccount, 
        whenAllHaveBeenCreated);

    function createEachCollectorAccount(collectorAccount, internalCallback){
        Collector.create(collectorAccount, function (errorOnCreating, collector) {

            const roleName = 'collector';
            const Role = app.models.Role;
            const RoleMapping = app.models.RoleMapping;
            const principalConnectionObject = {
                principalType: RoleMapping.USER,
                principalId: collector.id
            };


            Role.findOne({ name: roleName }, function (err, role) {
                if (err) throw err;

                // if not found, create it
                else if (!role) {
                    Role.create({ name: roleName }, function (err, role) {
                        if (err) throw err;
                        else {
                            role.principals.create(principalConnectionObject, function (errorOnPrincipalLinkCreation, principal) {
                                internalCallback(true);
                            })
                        }
                    })
                }
                else {

                    role.principals.create(principalConnectionObject, function (errorOnPrincipalLinkCreation, principal) {
                        internalCallback(true);
                    })
                }
            })
        });
    }

    function whenAllHaveBeenCreated(err, collectors){
        callback();
    }
}