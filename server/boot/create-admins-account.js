const fs = require('fs');
const async = require('async');

module.exports = function (app, callback) {
    // get models needed
    const Admin = app.models.Admin;

    // get admins server configuration
    const adminsAccount = require('../admins');

    if (!adminsAccount.accounts) adminsAccount.accounts = [];

    async.filter(
        adminsAccount.accounts,
        createEachAdminAccount,
        whenAllHaveBeenCreated);

    function createEachAdminAccount(adminAccount, internalCallback) {
        Admin.create(adminAccount, function (errorOnCreating, admin) {

            const roleName = 'admin';
            const Role = app.models.Role;
            const RoleMapping = app.models.RoleMapping;
            const principalConnectionObject = {
                principalType: RoleMapping.USER,
                principalId: admin.id
            };

            Role.findOne({ name: roleName}, function (err, role) {
                if (err) throw err;

                // if not found, create it
                else if (!role) {
                    Role.create({ name: roleName}, function (err, role) {
                        if (err) throw err;
                        else {
                            role.principals.create(principalConnectionObject, function (errorOnPrincipalLinkCreation, principal) {
                                internalCallback(true);
                            })
                        }
                    })
                }
                else {

                    role.principals.create(principalConnectionObject, function (errorOnPrincipalLinkCreation, principal) {
                        internalCallback(true);
                    })
                }
            })
        });
    }

    function whenAllHaveBeenCreated(err, admins) {
        callback();
    }
}