'use strict';

const async = require('async');
const roles = ['admin', 'client', 'bar', 'collector'];

module.exports = function(app, callback){

    // get model
    const Role = app.models.Role;

    // process async for each Role creation process
    async.filter(roles, function(roleName, cb) {

        // try find role...
        Role.findOne({name:roleName}, function(err, role){
            if (err) throw err;

            // if not found, create it
            else if (!role){
                Role.create({name:roleName}, function(err, role){
                    if (err) throw err;
                    else cb(null, true);
                })
            }
            else {
                cb(null, true);
            }
        })
    },callback);

}