module.exports = function(app){

    if (process.env.NODE_ENV === 'production'){
        process.env.RECOVER_PASSWORD_DOMAIN = 'checkbeer.com.br';
    }
    else {
        process.env.RECOVER_PASSWORD_DOMAIN = app.get('host') + ':' + app.get('port').toString();
    }
}