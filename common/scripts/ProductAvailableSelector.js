"use strict";



function RemoveAllRetailersThatAreNotAvailableAndReturnIfHaveAnyRetailers(product) {

    if (product !== null && product.retailers !== null && product.retailers.length > 0) {
        product.retailers = product.retailers.filter(productRetailer => productRetailer.available);
        return product.retailers.length > 0;
    }

    return false;
}

module.exports = RemoveAllRetailersThatAreNotAvailableAndReturnIfHaveAnyRetailers;