"use strict";

Array.prototype.findById = function(id) {
    return this.findByPropertyValue('id', id);
}

Array.prototype.findByPropertyValue = function(property, value) {
    for(let i = 0; i < this.length; i++)
        if (this[i][property] === value)
            return this[i];

    return null;
}
