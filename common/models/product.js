'use strict';

const async = require('async');
const app = require('../../server/server');
const looproductRightack = require('loopback');
const GeoPoint = looproductRightack.GeoPoint;
const TransverserRetailersToBrands = require('../libs/TransverserRetailersToBrands');
const TransverserRetailersToBestPriceMatchProducts = require('../libs/TransverserRetailersToBestPriceMatchProducts');

module.exports = function (Product) {

    // Show products highlights
    Product.highlights = function (lat, lng, radius, unit, respondeWithBestPriceMatchProducts) {

        findNearRetailers(lat, lng, radius, unit, whenFoundRetailers);

        function whenFoundRetailers(errorOnFinding, retailers) {
         
            if (errorOnFinding) respondeWithBestPriceMatchProducts(errorOnFinding);
            else TransverserRetailersToBestPriceMatchProducts(retailers, respondeWithBestPriceMatchProducts);            
        }
    };

    Product.remoteMethod(
        'highlights',
        {
            accepts: [
                { arg: 'lat', type: 'number', required: true },
                { arg: 'lng', type: 'number', required: true },
                { arg: 'radius', type: 'number', required: false, default: 1000 },
                { arg: 'unit', type: 'string', required: false, default: 'meters' }
            ],
            http: { path: '/highlights', verb: 'get' },
            returns: { arg: 'data', type: ['Product'], root: true }
        }
    );

    /**
     * Search near registered products within radius of provided latitude and longitude
     * and return their brands showing first highlighted ones. 
     */
    Product.near = function (lat, lng, radius, unit, respondWithBrands) {
        
        findNearRetailers(lat, lng, radius, unit, whenRetailerResponseHappen);
        
        function whenRetailerResponseHappen(errorOnFindRetailers, retailers) {

            if (errorOnFindRetailers) respondWithBrands(errorOnFindRetailers);
            else TransverserRetailersToBrands(retailers, finalBrandsTreat);
        }

        function finalBrandsTreat(brands) {

            brands.sort(function(brandLeft, brandRight) {
                if (isNullOrUndefined(brandLeft)) return -1;
                else if (isNullOrUndefined(brandRight)) return 1;
                else if (brandLeft.highlight && !brandRight.highlight) return -1;
                else if (!brandLeft.highlight && brandRight.highlight) return 1;
                else return 1;
            });
            
            respondWithBrands(false, brands);
        }
    };

    // 'near' Remote Method configutaion 
    Product.remoteMethod(
        'near',
        {
            accepts: [
                { arg: 'lat', type: 'number', required: true },
                { arg: 'lng', type: 'number', required: true },
                { arg: 'radius', type: 'number', required: false, default: 2000 },
                { arg: 'unit', type: 'string', required: false, default: 'meters' }
            ],
            http: { path: '/near', verb: 'get' },
            returns: { arg: 'data', type: ['Brand'], root: true }
        }
    );


    function findNearRetailers(lat, lng, radius, unit, callbackWhenRetailersFound) {
        const Retailer = app.models.Retailer;
        const currentLocation = new GeoPoint({ lat: lat, lng: lng });
        const unitType = unit || 'meters';

        const locationQuery = {
            include: [
                {
                    products: {
                        product: [
                            {
                                brandLine: 'brand'
                            },
                            {
                                retailers: [
                                    'retailer'
                                ]
                            }
                        ]
                    }
                }
            ],
            where: {
                location: {
                    near: currentLocation,
                    maxDistance: radius,
                    unit: unitType
                }
            }
        };

        Retailer.find(locationQuery, callbackWhenRetailersFound);
    }


    Product.productsWithPrice = function (page, limit, respondWithProductRetailersFound) {

        const ProductRetailer = app.models.ProductRetailer;

        const allOrWithLimit = {
            include: [
                {
                    product: {
                        brandLine: ['brand']
                    }
                },
                'retailer'
            ]
        }

        if (page > 0 && limit > 0) {
            allOrWithLimit['skip'] = page * limit;
            allOrWithLimit['limit'] = limit;
        }

        ProductRetailer.find(allOrWithLimit, respondWithProductRetailersFound);
    }

    Product.remoteMethod(
        'productsWithPrice',
        {
            accepts: [
                { arg: 'page', type: 'number', required: false, default: 0 },
                { arg: 'limit', type: 'number', required: false, default: 0 }
            ],
            http: { path: '/productsWithPrice', verb: 'get' },
            returns: { arg: 'data', type: ['Product'], root: true }
        }
    );

    // delete the relation with retailer
    Product.observe('before delete', function(ctx, next) {
        
        const ProductRetailer = app.models.ProductRetailer;
        const destroyClause = {
            productId : ctx.where.id
        };

        ProductRetailer.destroyAll(destroyClause, () => {next();});
    });
};

function isNullOrUndefined(theObjectToTest) {
    return theObjectToTest === null || theObjectToTest === undefined; 
}