'use strict';

const disableAllMethods = require('../libs/helpers').disableAllMethods;

module.exports = function(Admin) {
    let methodsToAllow = ['login','logout']
    // Disable all methods for admin, but login for production and create and login for development
    if (process.env.NODE_ENV === ('development')){
        console.log('Using admin with create for development.')
        methodsToAllow.push('create');
    }
    
    disableAllMethods(Admin, methodsToAllow);
};
