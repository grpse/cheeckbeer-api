'use strict';
let GeoPoint = require('loopback').GeoPoint;
const app = require('../../server/server');

module.exports = function(Retailer) {

	Retailer.near = function(lat, lng, distance, cb) {
		let point = new GeoPoint({lat: lat, lng: lng});
		let filterQuery = {
			// fields: ['imageId','highlight','name'],
			include: {
				relation: 'brandLines',
				scope: {
					// fields: ['imageId','name'],
					include: {
						relation: 'products',
						scope: {
							// fields: ['imageId','name', 'sku', 'units', 'packing'],
							include: {
								relation:'retailers',
								scope: {
									// fields: ['available','condition','price'],
									include:{
										relation:'retailer',
										scope: {
											where: {
		                						location: {
		                    						near: point,
		                    						maxDistance: distance,
		                    						unit: 'meters'
		                						}
		            						}
	            						}
									}									
								}
							}
						},
					}
				}
			}
		} //hadouken!!

		app.models.Brand.find(filterQuery, cb);
		
	};

	Retailer.remoteMethod(
		'near', 
		{
			accepts: [
				{arg: 'lat', type: 'number'},
				{arg: 'lng', type: 'number'},
				{arg: 'distance', type: 'number'}
			],
			http: { path: '/near', verb: 'get' },
			returns: { arg: 'data', type: ['Retailer'], root: true }
		}
	);


	// delete the relation with retailer
    Retailer.observe('before delete', function(ctx, next) {
        
        const ProductRetailer = app.models.ProductRetailer;
        const destroyClause = {
            retailerId : ctx.where.id
        };

        ProductRetailer.destroyAll(destroyClause, () => {next();});
    });

};
