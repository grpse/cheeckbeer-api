'use strict';

const disableAllMethods = require('../libs/helpers').disableAllMethods;
const GetRecoveryEmailBody = require('../libs/GetRecoveryEmailBody');

module.exports = function(Client) {
    disableAllMethods(Client,['login','logout','reset', 'findById', 'findOne','__findById__bookmarks','__destroyById__bookmarks', '__updateById__bookmarks','__exists__bookmarks','__link__bookmarks','__get__bookmarks', '__create__bookmarks','__update__bookmarks','__destroy__bookmarks', '__unlink__bookmarks','__count__bookmarks', '__delete__bookmarks',]);
    
    /**
     * Register a Client with normal data or facebook data
     */
    Client.register = function(data, cb){
        const clientData = {
            name : data.name || (data.facebook ? data.facebook.name : ''),
            type : data.type === 'client' ? 'client' : 'bar',
            email : data.email || (data.facebook ? data.facebook.email : ''),
            password : data.password || (data.facebook ? data.facebook.id : ''),
            birthday : data.birthday || (data.facebook ? data.facebook.birthday : ''),
            phone : data.phone || (data.facebook ? data.facebook.phone : ''),
            address : data.address || (data.facebook ? data.facebook.address : ''),
            facebook : data.facebook
        }

        Client.create(clientData, function(err, client){
            client.password = undefined;
            cb(err, client);
        });
    };

    Client.remoteMethod(
        'register',
        {
            accepts : [
                {arg : 'data', type : 'Client', http : {source:'body'}}
            ],
            http : {path : '/register', verb : 'post'},
            returns : {arg : 'data', type : 'Client', root:true}
        }
    );

    /**
     * Get my own profile data
     * 
     */
    Client.profile = function(req, cb){
        if(req.accessToken && req.accessToken.userId){
            Client.findById(req.accessToken.userId, cb);
        }
        else cb(true);
    };

    Client.remoteMethod(
        'profile',
        {
            accepts : [
                {arg: 'req', type: 'object', http: function(ctx) {return ctx.req}}
            ],
            http : {path: '/profile', verb: 'get'},
            returns : { arg: 'data', type:'Client', root:true}
        }
    );

    //send password reset link when requested
    Client.on('resetPasswordRequest', function(info) {
        const CheckBeerMail = Client.app.models.CheckBeerMail;
        var domain = process.env.RECOVER_PASSWORD_DOMAIN;
        var html = GetRecoveryEmailBody(info.accessToken.id);

        const mailObject = {
            to: info.email,
            from: 'recover@checkbeer.com',
            subject: 'Password reset',
            html: html
        };

        CheckBeerMail.send(mailObject, function(err) {
            if (err) return console.log('> error sending password reset email');
            console.log('> sending password reset email to:', info.email);
        });
    });

};