"use strict";

const fs = require('fs');
const path = require('path');

module.exports = function(accessToken) {
    
    const filePath = path.join(__dirname, "..", "..", "server", "recovery-email.html");
    const bodyContent = fs.readFileSync(filePath, "utf8");
    return bodyContent;
};