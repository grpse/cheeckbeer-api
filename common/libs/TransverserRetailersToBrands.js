"use strict";

const async = require('async');
const fs = require('fs');
// Array extension
require('../scripts/ArrayExtension.js');
const RemoveAllRetailersThatAreNotAvailableAndReturnIfHaveAnyRetailers = require('../scripts/ProductAvailableSelector.js');

module.exports = function (retailers, respondeWithBrands) {

    const retailersPopulated = JSON.parse(JSON.stringify(retailers || []));

    let brands = [];

    retailersPopulated.forEach(retailer => {
        
        retailer.products.forEach(productRetailer => {

            let brand = productRetailer.product.brandLine.brand;
            let brandLine = productRetailer.product.brandLine;
            let product = productRetailer.product;

            removeCircularReference(brand, brandLine, product);

            brand = tryGetBrandFromBrandsArray(brand, brands);
            tryInsertBrandIntoBrandsArray(brand)

            brandLine = tryGetBrandLineFromBrand(brandLine, brand);
            tryInsertBrandLineIntoBrand(brandLine, brand);

            product = tryGetProductFromBranLine(product, brandLine);

            if (RemoveAllRetailersThatAreNotAvailableAndReturnIfHaveAnyRetailers(product)){
                tryInsertProductIntoBrandLine(product, brandLine);
            }                      
        });
    });
    
    respondeWithBrands(brands);

    function tryGetBrandFromBrandsArray(brand, brands) {
        let brandRet = brands.findById(brand.id);
        if (brandRet !== null){
            return brandRet;
        }
        else return brand;
    }

    function tryGetBrandLineFromBrand(brandLine, brand) {
        let brandLineRet = brand.brandLines ? brand.brandLines.findById(brandLine.id) : null;
        if (brandLineRet) {
            return brandLineRet;
        }
        else return brandLine;
    }
    
    function tryGetProductFromBranLine(product, brandLine) {
        let productRet = brandLine.products ? brandLine.products.findById(product.id) : null;
        if (productRet) {
            return productRet;
        } 
        else return product;
    }

    function tryInsertBrandIntoBrandsArray(brand) {
        tryInsertThingIntoArray(brand, brands);
    }

    function tryInsertBrandLineIntoBrand(brandLine, brand) {
        brand.brandLines = tryInsertThingIntoArray(brandLine, brand.brandLines);
    }

    function tryInsertProductIntoBrandLine(product, brandLine) {
        brandLine.products = tryInsertThingIntoArray(product, brandLine.products);
    }

    function tryInsertThingIntoArray(thing, array) {

        if (array instanceof Array) {

            //let thingInside = array.findById(thing.id);
            if (array.indexOf(thing) < 0){
                array.push(thing);
            }
            return array;
        }
        else return [thing];
    }

    function removeCircularReference(brand, brandLine, product) {
        product.brandLine = undefined;
        brandLine.brand = undefined;
    }
};

