"use strict";

// Array extension
require('../scripts/ArrayExtension.js');
const RemoveAllRetailersThatAreNotAvailableAndReturnIfHaveAnyRetailers = require('../scripts/ProductAvailableSelector.js');

module.exports = function(retailers, respondeWithProductsAndNoError) {

    let productsArray = [];

    const retailersPopulated = JSON.parse(JSON.stringify(retailers || []));

    retailersPopulated.forEach( retailer => {

        retailer.products.forEach( productRetailer => {

            let product = productRetailer.product;
            if (product !== null && RemoveAllRetailersThatAreNotAvailableAndReturnIfHaveAnyRetailers(product)) {
                tryInsertOnceProductIntoProductsResponseArray(product);
            }
        });
    });

    function trySortProductsArrayByItsBestPriceMatchCostByml(products) {
        
        if (products instanceof Array) {

            products.sort( (productLeft, productRight) => {

                if (productLeft === null) return 1;
                else if (productRight === null) return -1;
                else if (productLeft === null && productRight === null) return 0;
                else {

                    
                    let leftPriceByMl = getPriceByMlFromProductWithAtLeastOneRetailer(productLeft);
                    let rightPriceByMl = getPriceByMlFromProductWithAtLeastOneRetailer(productRight);
                    
                    return leftPriceByMl - rightPriceByMl;
                }
            });
        }
    }

    function tryInsertOnceProductIntoProductsResponseArray(product) {
        
        let productRet = productsArray.findById(product.id);
        if (productRet === null) {
            productsArray.push(product);
        }        
    }

    function getPriceByMlFromProductWithAtLeastOneRetailer(product) {
        if (product.retailers instanceof Array && product.retailers.length > 0) {
            
            try {
                trySortProductRetailersRelationByItsBestPrice(product);
                let bestPrice = product.retailers[0].price;

                return bestPrice / product.sku;
            }
            catch(e) {
                // will return with max value to let it be the last in the list
            }                        
        }

        return Number.MAX_VALUE;
    }

    function trySortProductRetailersRelationByItsBestPrice(product) {

        if (product.retailers instanceof Array) {
            product.retailers.sort(function(retailerLeft, retailerRight) {
                if (isNullOrUndefined(retailerLeft.price)) return 1;
                else if (isNullOrUndefined(retailerRight.price)) return -1;
                else return retailerLeft.price - retailerRight.price;
            });
        }
    }

    function selectAmountOfProducts(amount, products) {
               
        let productsResponseArray = [];

        for(let i = 0; i < amount; i++) {

            if (i >= products.length) break;
            else {

                let product = products[i];
                productsResponseArray.push(product);
            }
        }

        return productsResponseArray; 
    }

    function isNullOrUndefined(testObject) {
        return testObject === null || testObject === undefined;
    }


    trySortProductsArrayByItsBestPriceMatchCostByml(productsArray);
    productsArray = selectAmountOfProducts(20, productsArray);
    respondeWithProductsAndNoError(0, productsArray);
};