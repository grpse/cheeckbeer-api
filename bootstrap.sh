#!/usr/bin/env bash

#essentials instalation
cd /vagrant/
sudo apt-get update
sudo apt-get install -y build-essential libssl-dev

#installs nvm
export NVM_DIR="/home/vagrant/.nvm"
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
source $NVM_DIR/nvm.sh

#installs mongodb
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org

nvm install 6 #installs node 6
npm install -g pm2 #installs pm2 file
npm install #installs dependencies on package.json

pm2 start process.yml --env production #starts process.yml on pm2 with production flag

# #install nginx
# sudo apt-get update
# sudo apt-get install -y nginx