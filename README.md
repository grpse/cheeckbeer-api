# CheckBeer API

## Stack:
- Node.js
- ORM database based in models with MongoDB
- Loopback Framework

## Running
- git clone https://bitbucket.com/criarme/checkbeer-api.git
- cd checkbeer-api
- npm install
- npm start
- npm test, for run testing
- npm run-script start-dev, for start development environment on linux


