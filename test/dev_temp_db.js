// no image provided for brands
// BE AWARE! Don't set id, because set id will going to use
module.exports = {
    "brands" : [
        {
            "name" : "Skol",
            "highlight" : true,
        },
        {
            "name" : "Antarctica",
            "highlight" : true
        },
        {
            "name" : "Proibida",
            "highlight" : true
        },
        {
            "name" : "Bohemia",
            "highlight" : true
        },
        {
            "name" : "Sol",
            "highlight" : false
        },
        {
            "name" : "Devassa",
            "highlight" : false
        },
        {
            "name" : "Crystal",
            "highlight" : false
        },
        {
            "name" : "Itaipava",
            "highlight" : false
        },
        {
            "name" : "Kaiser",
            "highlight" : false
        },
        {
            "name" : "Nova Schin",
            "highlight" : false
        },
        {
            "name" : "Brahma",
            "highlight" : false
        }
    ],
    "brandLines" : [
        {
            "name" : "Skol Beats"
        },
        {
            "name" : "Antarctica Original"
        },
        {
            "name" : "Proibida Puro Malte"
        },
        {
            "name" : "Bohemia Original"
        },
        {
            "name" : "Sol Too Strange To Be a Beer"
        },
        {
            "name" : "Devassa a Sandy?"
        },
        {
            "name" : "Crystal nothing else to say"
        },
        {
            "name" : "Itaipava Comum"
        },
        {
            "name" : "Kaiser Bock"
        },
        {
            "name" : "Nova Schin, the first non-flavored beer of the world"
        },
        {
            "name" : "Brahma Puro Malte"
        }
    ],
    "products" : [
        {
            "name" : "Skol Beats Sense",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Skol Beats Spirit",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Antarctica Blah",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Antarctica Blue",
            "sku" : 225,
            "units" : 1
        },
        {
            "name" : "Proibida Puro Malte Premium",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Proibida Puro Malte Delirium",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Bohemia Nice Shot",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Bohemia Rhapsody",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Sol Shot",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Sol Head Shot",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Devassa Is You Sandy?",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Devassa don't even matter?",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Crystal NOOOO!!!",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Crystal better be a coffee drinker!",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Itaipava no body drinks",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Itaipava even actors on advertising",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Kaiser Bock Bock",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Kaiser Who is?",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Nova Schin not that good",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Nova Schin choose other things",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Brahma only special ones",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Brahma yeah yeah!",
            "sku" : 255,
            "units" : 1
        },
        {
            "name" : "Skol Beats Secret",
            "sku" : 255,
            "units" : 1
        }
    ],
    "retailers" : [
        {
            "name" : "Extra",
            "address" : "R. Boa Vista, 523 - Boa Vista, São Caetano do Sul - SP, 09572-300",
            "location" : {
                "lat" : -23.642654,
                "lng" : -46.560726
            }
        },
        {
            "name" : "Madrid Supermercados",
            "address" : "R. Martim Francisco, 777 - Vila Buarque, São Paulo - SP, 01226-000",
            "location" : {
                "lat" : -23.543347, 
                "lng" : -46.654479
            }
        },
        {
            "name" : "Dia Supermercados",
            "address" : "R. do Paraíso, 812 - Paraíso, São Paulo - SP",
            "location" : {
                "lat" : -23.575188,
                "lng" : -46.635667
            }
        },
        {
            "name" : "Master Supermercados",
            "address" : "R. Rio Grande, 450 - Vila Mariana, São Paulo - SP, 04018-001",
            "location" : {
                "lat" : -23.588533,
                "lng" : -46.642504
            }
        }
    ],
    "bars" : [
        {
            "name" : "Zé Bars",
            "description" : "This first Zé bars that runs on the world.",
            "address" : "Avenida Senador Nei Brito, nº 540",
            "location" : {
                "lat" : -29.925826, 
                "lng" : -51.037017
            },
            "phone" : "5130304040",
            "website" : "www.zebars.com.br",
            "email" : "contato@zebars.com.br",
            "facebook": "/zebarsoficial",
            "instagram" : "@zebarsoficial"
        }
    ],
    "barImages" : [
        {
            
        }
    ],
    "barAds" : [
        {

        }
    ],
    "banners" : [
        {
            "description" : "This is a good thing!",
            "link" : "www.zebars.com.br/goodthing"
        }
    ],
    "client_position" : {
        "lat" : -23.638334,
        "lng" : -46.566773
    },
    "distante_location" : {
        "lat" : -29.908066, 
        "lng" : -51.045228
    },
    "clientData" : {
        "email" : "gilbertoribeiropazdarosa@gmail.com",
        "password" : "test",
        "birthday" : "1990-12-02",
        "name" : "Test Client"
    },
    "priceMatches" : [
        900,
        800,
        700,
        123,
        123,
        123,
        123,
        123,
        123,
        123,
        123,
        123,
        123,
        123,
        60,
        60,
        60,
        60,
        60,
        60,
        60,
        2
    ]
}