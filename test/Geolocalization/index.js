'use strict';

const should = require('should');

module.exports = function (app) {

    let server = {};

    describe('Geolocalization', function () {

        before(function (done) {
            //get admin 0 credentials
            global.admin0 = Object.assign({}, require('../../server/admins.json')['accounts'][0]);


            // ensure that 'done' was called
            server = app.listen(function () {
                // login with admin0
                Request
                    .post('/api/Admins/login')
                    .send(admin0)
                    .end(function (err, res) {
                        if (err) throw err;

                        // save admin token
                        admin0.token = res.body.id;
                        done();
                    });
            });
        });

        after(function (done) {
            server.close(done);
        });

        it('GET /api/products/near. Should get near products.', shouldGetNearProducts);
        it('GET /api/products/near. Should find nothing from a distante location.', shouldFindNothingFromDistanteLocation);
        it('GET /api/bars/. Should find near bars.', shouldFindNearBars);
    });

    function shouldGetNearProducts(done) {
        const curPos = dev_temp_db.client_position;
        let finalUrl = '/api/products/near';
        finalUrl += '?lat=:lat&lng=:lng&radius=:radius&unit=:unit';
        finalUrl = finalUrl
            .replace(':lat', curPos.lat)
            .replace(':lng', curPos.lng)
            .replace(':radius', 2000)
            .replace(':unit', 'meters');

        Request
            .get(finalUrl)
            .end(function (err, res) {

                if (err) throw err;
                
                res.body.should.be.instanceOf(Array);
                res.body[0].name.should.be.equal('Antarctica');

                done();
            });
    }

    function shouldFindNothingFromDistanteLocation(done) {
        const curPos = dev_temp_db.distante_location;
        let finalUrl = '/api/products/near';
        finalUrl += '?lat=:lat&lng=:lng&radius=:radius&unit=:unit';
        finalUrl = finalUrl
            .replace(':lat', curPos.lat)
            .replace(':lng', curPos.lng)
            .replace(':radius', 1)
            .replace(':unit', 'meters');

        Request
            .get(finalUrl)
            .end(function (err, res) {
                if (err) throw err;

                res.body.should.be.instanceOf(Array);
                res.body.length.should.be.equal(0);

                done();
            });
    }

    function shouldFindNearBars(done) {
        const curPos = dev_temp_db.distante_location;
        
        let finalUrl = '/api/bars/';
        
        const filterObject = {
            "where": {
                "location": {
                    "near": {
                        "lat" : curPos.lat, //-29.925826, 
                        "lng" : curPos.lng//-51.037017
                    },
                    "unit": "meters",
                    "maxDistance": 0
                },
                "include": [
                    "barImages"
                ]
            }
        };

        //"[where][location][near]=-29.925826,-51.037017"

        finalUrl += "?filter" + JSON.stringify(filterObject);

        Request
            .get(finalUrl)
            .end(function (err, res) {
                if (err) throw err;

                res.body.should.be.instanceOf(Array);
                res.body.length.should.be.equal(1);

                done();
            });
    }
}