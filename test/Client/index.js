'use strict';
const should = require('should');
const async = require('async');

module.exports = function(app){
    
    let server = {};
    let client = {};

    describe('Client', function(){
        before(function(done){
            // ensure that 'done' was called
            server = app.listen(done); 
        });

        after(function(done){
            server.close(done);
        });

        it('POST /api/clients/register. Should register a client.', shouldRegisterClient);
        it('POST /api/clients/login. Should login a client.', shouldLoginClient);
        it('POST /api/clients/profile. Should get client profile.', shouldGetClientProfile);
        it('POST /api/clients/{id}/bookmarks. Should create a bookmark.', shouldCreateBookmark);
        it('GET  /api/clients/{id}/bookmarks. Should get bookmarks.', shouldGetBookmarks);
        it('POST /api/clients/logout. Should logout an admin account.', shouldLogoutClientAccount);
        it('POST /requestPasswordReset. Should send recovery email.', shouldSendRecoveryEmail);
    });

    function shouldRegisterClient(done){
        Request
            .post('/api/clients/register')
            .send(dev_temp_db.clientData)
            .end(function(err, res){

                if (err) throw err;

                client = dev_temp_db.clientData;
                client.id = res.body.id;
                res.body.should.have.property('id');

                done();
            });
    }

    function shouldLoginClient(done){
        Request
            .post('/api/clients/login')
            .send(client)
            .end(function(err, res){

                if (err) throw err;

                client.token = res.body.id;
                res.body.userId.should.be.equal(client.id);               

                done();
            });
    }

    function shouldGetClientProfile(done) {
        Request
            .get('/api/clients/profile?access_token='+client.token)
            .end(function(err, res){

                if (err) throw err;
                
                res.body.id.should.be.equal(client.id);               

                done();
            });
    }

    function shouldCreateBookmark(done){
        const bookmark = {
            type : 'product',
            productId : 1,
            barId : 1
        };

        
        let finalUrl = '/api/clients/:id/bookmarks'.replace(':id', client.id);
        finalUrl += '?access_token='+client.token;

        Request
            .post(finalUrl)
            .send(bookmark)
            .end(function(err, res){

                if(err) throw err;

                res.body.should.have.property('id');
                res.body.type.should.be.equal(bookmark.type);

                done();
            });
    }

    function shouldGetBookmarks(done){
        let finalUrl = '/api/clients/:id/bookmarks'.replace(':id', client.id);
        finalUrl += '?access_token='+client.token;
        Request
            .get(finalUrl)
            .end(function(err, res){

                if(err) throw err;

                res.body.should.be.instanceOf(Array);
                res.body[0].should.have.property('id');

                done();
            });
    }

    function shouldLogoutClientAccount(done){
        let finalUrl = '/api/clients/logout'
        finalUrl += '?access_token='+client.token;
        Request
            .post(finalUrl)
            .expect(204)
            .end(function(err, res){

                if(err) throw err;

                done();
            });
    }

    function shouldSendRecoveryEmail(done){
        let finalUrl = '/requestPasswordReset';
        let recoveryData = {
            email : dev_temp_db.clientData.email
        };
        Request
            .post(finalUrl)
            .send(recoveryData)
            .end(function(err, res){
                if(err) throw err;

                done();
            })
    }
}
    