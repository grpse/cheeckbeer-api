'use strict';
const should = require('should');
const async = require('async');

module.exports = function(app){
    
    let server = {};
    let client = {};

    describe('Admin', function(){
        before(function(done){
            //get admin 0 credentials
            global.admin0 = Object.assign({}, require('../../server/admins.json')['accounts'][0]);
            // ensure that 'done' was called
            server = app.listen(done);
        });

        after(function(done){
            server.close(done);
        });

        it('POST /api/admins/login. Should login to an admin account that was on startup created.', shouldLoginAnAdmin);
        it('POST /api/banners. Should create a banner.', shouldCreateBanner);
        it('POST /api/admins/logout. Should logout an admin account.', shouldLogoutAdminAccount);
    });


    function shouldLoginAnAdmin(done){
        
        this.timeout(10000);

        Request
            .post('/api/Admins/login')
            .send(admin0)
            .end(function(err, res){
                
                if (err) throw err;
                res.body.should.have.property('userId');
                res.body.should.have.property('id');
                admin0.token = res.body.id;
                done();
            });
    }

    function shouldCreateBanner(done){

        this.timeout(10000);

        Request
            .post('/api/Admins/login')
            .send(admin0)
            .end(function(err, res){
                
                if (err) throw err;
                res.body.should.have.property('userId');
                res.body.should.have.property('id');
                admin0.token = res.body.id;
                
                async.each(dev_temp_db.banners, function(banner, internalCB){
                    Request
                        .post('/api/banners')
                        .set('Authorization', admin0.token)
                        .send(banner)
                        .end(function(err, res){
                            if(err) throw err;
                            
                            res.body.description.should.be.equal(banner.description);
                            res.body.should.have.property('id');

                            internalCB(null, true);
                        })
                }, done);
        });

    }

    function shouldLogoutAdminAccount(done){
        Request
            .post('/api/Admins/logout?access_token='+admin0.token)
            .end(function(err, res){
                if(err) throw err;
                done();
            });
    }
}
    