"use strict";

const should = require('should');
const async = require('async');
const AUTHORIZATION = 'Authorization';

let brands = [];
let brandLines = [];
let products = [];
let retailers = [];
let productRetailers = [];

module.exports = function(app){
    // temporary storage for beers

    let server = {};

    describe('Beers', function(){

        before(function(done){
            //get admin 0 credentials
            global.admin0 = Object.assign({}, require('../../server/admins.json')['accounts'][0]);

            
            // ensure that 'done' was called
            server = app.listen(function(){
                // login with admin0
                Request
                    .post('/api/Admins/login')
                    .send(admin0)
                    .end(function(err, res){
                        if(err) throw err;

                        // save admin token
                        admin0.token = res.body.id;
                        done();
                    });
            }); 
        });

        after(function(done){
            server.close(done);
        });

        it ('POST /api/brands. Should create all beers brands.', shouldCreateAllBeerBrands);
        it ('POST /api/brands/{id}/brandLines. Should create 1 brand line for each brand.', shouldCreateBrandLines);
        it ('GET /api/brands. Should get all brands created.', shouldGetAllBrandsCreated);
        it ('GET /api/brands/{id}/brandLines. Should get brand lines for each brand.', shouldGetAllBrandLines);
        it ('POST /api/products/. Should create 2 beers for each brand line.', shouldCreate2BeersForeachBrandLine);
        it ('GET /api/products. Should get all beers.', shouldGetAllBeers);
        it ('POST /api/retailers. Should create retailers.', shouldCreateRetailers);
        it ('POST /api/products/{id}/retailers. Should create a relation product retailers.', shouldCreateRelationProductRetailers);
        it ('GET /api/products/highlights. Should get products ordered by price/ml.', shouldGetHighlights);
        it ('DELETE /api/products/{id}. Should delete a product and cascaded delete relation with retailer.', shouldDeleteProductRetailerRelationOnCascade)
        it ('POST /api/products/{id}. Shuld create new product to a same brand line and check if "near" products return some brand line duplicated.', shouldNotHaveDuplicatedBrandlineWhenCreateNewProductForTheSameBrandline);
        it ('POST /api/products/{id}/retailers. Should create new retailer relation and "near" not return duplicated brand line.', shouldNotHaveDuplicatedBrandlineWhenCreateNewRetailerRelation);
        it ('GET /api/products/highlights?lat=-25.123456&lng=-46.654321&radius=0&unit=meters. Should return only 20 products.', shouldReturnOnly20ProductsOnHighlights);
        it ('GET /api/products/highlights?lat=-25.123456&lng=-46.654321&radius=0&unit=meters. Should return only available products for highlights endpoint.', shouldReturnOnlyAvailableProductsForHighlights);
        it ('GET /api/products/near?lat=-25.123456&lng=-46.654321&radius=0&unit=meters. Should return only available products for near endpoint (depends on first).', shouldReturnOnlyAvailableProductsForNear);
    });

    function shouldCreateAllBeerBrands(done){
        // create all brands and store it at 'brands' array
        async.filterSeries(dev_temp_db.brands, function(brand, internalCB){
            Request
                .post('/api/brands')
                .set('Authorization', admin0.token)
                .send(brand)
                .end(function(err, res){
                    if (err) throw err;

                    res.body.name.should.equal(brand.name);
                    res.body.should.have.property('id');

                    brands.push(res.body);

                    internalCB(null, true);
                });
        }, done);        
    }

    function shouldCreateBrandLines(done){

        // for each brand create a brand line
        async.filterSeries(brands, function(brand, internalCB){

            const brandIndex = brands.indexOf(brand);
            const brandLine = dev_temp_db.brandLines[brandIndex];
            //populate brandId
            brandLine.brandId = brand.id;
            const finalUrl = '/api/brands/:id/brandLines'.replace(':id', brand.id);

            Request
                .post(finalUrl)
                .set('Authorization', admin0.token)
                .send(brandLine)
                .end(function(err, res){
                    if(err) throw err;

                    res.body.should.have.property('id');
                    res.body.brandId.should.equal(brand.id);

                    brandLines.push(res.body);
                    
                    internalCB(null, true);
                });

        }, done);
    }

    function shouldGetAllBrandsCreated(done){

        // try to get all brands created
        Request
            .get('/api/brands')
            .end(function(err, res){
                if(err) throw err;
                
                res.body.should.instanceOf(Array);

                res.body.filter(function(brand){
                    return brand in brands;
                });

                done();
            })
    }

    function shouldGetAllBrandLines(done){

        // for each brand try to get brand lines available
        async.filterSeries(brands, function(brand, internalCB){

            const finalUrl = '/api/brands/:id/brandLines'.replace(':id', brand.id);

            Request
                .get(finalUrl)
                .end(function(err, res){

                    if(err) throw err;

                    res.body.should.be.instanceOf(Array);

                    // check if each brand line is inside brand lines array
                    const answer = res.body.filter(function(brandLine){
                        const ba = brandLines.filter(function(b){
                            return b.id === brandLine.id;
                        })[0];
                        return ba.id !== undefined;
                    });

                    answer.length.should.be.greaterThan(0);

                    internalCB(null, true);
                });
        }, done);
    }

    function shouldCreate2BeersForeachBrandLine(done){

        // create 2 products for each brand line
        async.filterSeries(dev_temp_db.products, function(product, internalCB){

            const productIndex = dev_temp_db.products.indexOf(product);
            const brandLine = brandLines[ productIndex % brandLines.length];
            product.brandLineId = brandLine.id;
            const finalUrl = '/api/products/';

            Request
                .post(finalUrl)
                .set('Authorization', admin0.token)
                .send(product)
                .end(function(err, res){
                    if(err) throw err;

                    res.body.brandLineId.should.equal(brandLine.id);
                    res.body.name.should.equal(product.name);
                    res.body.should.have.property('id');
                    products.push(res.body);

                    internalCB(null, true);
                });

        }, done);
    }

    function shouldGetAllBeers(done){
        Request
            .get('/api/products')
            .end(function(err, res){
                if(err) throw err;

                res.body.should.be.instanceOf(Array);
                
                const answer = res.body.filter(function(product){
                    
                    const ba = products.filter(function(p) {

                        return p.id === product.id;
                    })[0];
                    return ba.id !== undefined;
                });

                answer.length.should.be.greaterThan(0);

                done();
            });
    }

    function shouldCreateRetailers(done){
        async.filterSeries(dev_temp_db.retailers, function(retailer, internalCB){

            Request
                .post('/api/retailers')
                .set('Authorization', admin0.token)
                .send(retailer)
                .end(function(err, res){

                    if(err) throw err;

                    res.body.should.have.property('id');
                    res.body.name.should.be.equal(retailer.name);

                    retailers.push(res.body);

                    internalCB(null, true);
                })

        }, done);
    }

    function shouldCreateRelationProductRetailers(done){


        // for each product create a relation with retailer
        async.filterSeries(products, function(product, callbackForEachProcessingProductDone){
            
            const finalUrl = '/api/products/:id/retailers'.replace(':id', product.id);
            const productIndexForPriceMatchIndex = products.indexOf(product);
            const productIndex = productIndexForPriceMatchIndex;
            const price = dev_temp_db.priceMatches[productIndexForPriceMatchIndex];

            const retailerIndex = productIndex % retailers.length; 
            const retailer = retailers[retailerIndex];

            const productRetailer =   {
                "available": true,
                "condition": "Test condition",
                "price": price,
                "productId": product.id,
                "retailerId": retailer.id
            };

            Request
                .post(finalUrl)
                .set('Authorization', admin0.token)
                .send(productRetailer)
                .end(function(err, res){

                    if(err) throw err;

                    res.body.productId.should.be.equal(product.id);
                    res.body.retailerId.should.be.equal(retailer.id);
                    res.body.should.have.property('id');

                    productRetailers.push(res.body);

                    callbackForEachProcessingProductDone(null, true);
                });

        }, done);

    }

    function shouldGetHighlights(done){

        const curPos = dev_temp_db.client_position;
        let finalUrl = '/api/products/highlights';
        finalUrl += '?lat=:lat&lng=:lng&radius=:radius&unit=:unit';
        finalUrl = finalUrl
                        .replace(':lat', curPos.lat)
                        .replace(':lng', curPos.lng)
                        .replace(':radius', 2000)
                        .replace(':unit', 'meters');


        Request
            .get(finalUrl)
            .end(function(err, res){

                if(err) throw err;
                
                res.body.should.be.instanceOf(Array);
                
                // the [0] index product should be the best price match
                //console.log(res.body[0]);
                // the last index is the best price
                const bestPrice = dev_temp_db.priceMatches[20];
                res.body[0].retailers[0].price.should.be.equal(bestPrice);                

                done();
            });
    }

    function shouldDeleteProductRetailerRelationOnCascade(done) {

        const productId = products[products.length - 1].id;
        const finalProductsURL = '/api/products/:id'.replace(':id', productId);

        Request
            ['delete'](finalProductsURL)
            .set(AUTHORIZATION, admin0.token)
            .set('Content-Type', 'application/json')
            .end(handleProductDeletionVerifyIfRetailerRelationIsDeletedToo);
        
        function handleProductDeletionVerifyIfRetailerRelationIsDeletedToo(errorOrRequest, res) {

            if(errorOrRequest) throw errorOrRequest;

            const finalProductsWithPriceURL = '/api/products/productsWithPrice';

            Request
                .get(finalProductsWithPriceURL)
                .set('Content-Type', 'application/json')
                .end(verifyIfSomeOfTheProductRetailersReturnedHaveAnEmptyProduct);
        }

        function verifyIfSomeOfTheProductRetailersReturnedHaveAnEmptyProduct(errorOrRequest, res) {

            if (errorOrRequest) throw errorOrRequest;

            for(let i = 0; i < res.body.length; i++) {

                const productRetailer = res.body[i];
                productRetailer.product.should.have.property('id');
                productRetailer.product.should.have.property('name');
                productRetailer.product.should.have.property('units');
                productRetailer.product.should.have.property('sku');
            }

            done();
        }
    }

    function shouldNotHaveDuplicatedBrandlineWhenCreateNewProductForTheSameBrandline(done) {
        
        this.timeout(10000);

        const brandLineId = brandLines[0].id;
        const retailerId = retailers[0].id;
        const finalProductsUrl = '/api/products/'; 
        const product = {
            "name" : "Skol Beats Secret",
            "sku" : 255,
            "units" : 1,
            "packing" : "lata",
            "brandLineId" : brandLineId
        };

        Request
            .post(finalProductsUrl)
            .set(AUTHORIZATION, admin0.token)
            .send(product)
            .end(handleProductCreationResponse);

        function handleProductCreationResponse(errorOrRequest, res) {
            
            if (errorOrRequest) throw errorOrRequest;

            res.body.should.have.property('id');

            product.id = res.body.id;

            const finalProductRetailerURL = finalProductsUrl + product.id + '/retailers';
            const productRetailer = {
                "condition" : "",
                "available" : true,
                "price" : 500,
                "retailerId" : retailerId,
                "productId" : product.id
            };

            Request
                .post(finalProductRetailerURL)
                .send(productRetailer)
                .set(AUTHORIZATION, admin0.token)
                .end(handleProducRetailerCreation)
        }

        function handleProducRetailerCreation(errorOrRequest, res) {

            if (errorOrRequest) throw errorOrRequest;

            res.body.should.have.property('id');

            makeNearBrandsRequestWith(handleGetNearBrands);
        }


        function handleGetNearBrands(errorOrRequest, res) {
            
            if (errorOrRequest) throw errorOrRequest;
            
            const brands = JSON.parse(JSON.stringify(res.body));
            
            brands.should.be.instanceOf(Array);
            
            if (checkIfSomeOfTheBrandLinesIsDuplicated(brands)) done();
            else throw "Some of the brand lines are duplicated";
        }

        
    }

    function shouldNotHaveDuplicatedBrandlineWhenCreateNewRetailerRelation(done) {

        createNewProductRelationWithHandle(forProductRetailerRelationCreationRequest);

        function createNewProductRelationWithHandle(handler) {
            
            const product = products[0];
            const retailer = retailers[0];
            const finalUrlToCreateProductRetailerRelation = '/api/products/' + product.id + '/retailers';

            const productRelation = {
                "condition" : "",
                "available" : true,
                "price" : 500,
                "retailerId" : retailer.id,
                "productId" : product.id 
            };

            Request
                .post(finalUrlToCreateProductRetailerRelation)
                .send(productRelation)
                .set(AUTHORIZATION, admin0.token)
                .end(handler);
        }

        function forProductRetailerRelationCreationRequest(errorOrRequest, res) {

            if (errorOrRequest) throw errorOrRequest;

            if (errorOrRequest) throw errorOrRequest;

            res.body.should.have.property('id');

            makeNearBrandsRequestWith(handleGetNearBrands);
        }

        function handleGetNearBrands(errorOrRequest, res) {
            
            if (errorOrRequest) throw errorOrRequest;
            
            const brands = JSON.parse(JSON.stringify(res.body));
            
            brands.should.be.instanceOf(Array);
            
            if (checkIfSomeOfTheBrandLinesIsDuplicated(brands)) done();
            else throw "Some of the brand lines are duplicated";
        }
    }

    function shouldReturnOnly20ProductsOnHighlights(done) {

        const productsHighlightsURL = "/api/products/highlights?lat=-25.123456&lng=-46.654321&radius=0&unit=meters";

        Request
            .get(productsHighlightsURL)
            .end(handleProductsHighlightsRequest);
        
        function handleProductsHighlightsRequest(errorOrRequest, res) {

            if (errorOrRequest) throw errorOrRequest;

            if (verifyIfHaveOnly20Products(res.body)) done();
            else throw "Don't have 20 products.";
        }

        function verifyIfHaveOnly20Products(products) {
            return products.length === 20;
        }
    }

    function shouldReturnOnlyAvailableProductsForHighlights(done) {
        
        this.timeout(10000);

        const brandLineId = brandLines[0].id;

        const productData = {
            name : "BEER_NOT_AVAILABLE_1",
            sku : 255,
            units : 1,
            packing : "Lata",
            brandLineId : brandLineId
        }

        createProductAndRetailerRelationWithAvailableFalse(productData, handleProductRetailerRelationCreationRequestAndCheckIfProductCreatedIsReturned);


        function handleProductRetailerRelationCreationRequestAndCheckIfProductCreatedIsReturned(errorOnRequest, res) {
            
            if (errorOnRequest) throw errorOnRequest;

            makeHighlightsProductsRequestWith(function(errorOnRequest, res) {
                
                if (errorOnRequest) throw errorOnRequest;
                
                let products = res.body;

                let productSelected = products.filter(product => product.name === productData.name);
                if (productSelected.length === 0) done();
                else throw "Product not available have been returned";

            });
        }

    }

    function shouldReturnOnlyAvailableProductsForNear(done) {

        this.timeout(10000);

        const brandLineId = brandLines[0].id;

        const productData = {
            name : "BEER_NOT_AVAILABLE_2",
            sku : 255,
            units : 1,
            packing : "Lata",
            brandLineId : brandLineId
        }

        createProductAndRetailerRelationWithAvailableFalse(productData, handleProductRetailerRelationCreationRequestAndCheckIfProductCreatedIsReturned);


        function handleProductRetailerRelationCreationRequestAndCheckIfProductCreatedIsReturned(errorOnRequest, res) {
            
            if (errorOnRequest) throw errorOnRequest;

            makeNearBrandsRequestWith(function(errorOnRequest, res) {
                
                if (errorOnRequest) throw errorOnRequest;
                
                let brands = res.body;

                let brandLines = getAllBrandLinesFromBrands(brands);
                let products = getOnlyProductsFromBrandLines(brandLines);

                let productSelected = products.filter(product => product.name === productData.name);
                if (productSelected.length === 0) done();
                else throw "Product not available have been returned";

            });
        }
    }
}

function createProductAndRetailerRelationWithAvailableFalse(productData, andCallItWhenDone) {

    const productsUrl = '/api/products/';
    const retailer = retailers[0];

    Request
        .post(productsUrl)
        .set(AUTHORIZATION, admin0.token)
        .send(productData)
        .end(function(errorOnRequest, res) {

            if (errorOnRequest) throw errorOnRequest;

            res.body.should.have.property('id');

            const productId = res.body.id;
            const productRetailersUrl = productsUrl + productId + "/retailers";

            const productRetailer = {
                available : false,
                price : 0,
                condition : "",
                retailerId : retailer.id,
                productId : productId
            };

            Request
                .post(productRetailersUrl)
                .set(AUTHORIZATION, admin0.token)
                .send(productRetailer)
                .end(andCallItWhenDone);
        });
}

function makeHighlightsProductsRequestWith(handler) {

    const finalNearProducts = '/api/products/highlights?lat=-25.322500&lng=-46.123456&radius=0&unit=meters';

    Request
        .get(finalNearProducts)
        .end(handler);
}

function makeNearBrandsRequestWith(handler) {

    const finalNearProducts = '/api/products/near?lat=-25.322500&lng=-46.123456&radius=0&unit=meters';

    Request
        .get(finalNearProducts)
        .end(handler);
}

function checkIfSomeOfTheBrandLinesIsDuplicated(brands) {

    const brandLines = getAllBrandLinesFromBrands(brands);

    for(let i = 0; i < brandLines.length; i++) {
        
        const brandLineOutter = brandLines[i];

        for(let j = 0; j < brandLines.length && i !== j; j++) {

            const brandLineInner = brandLines[j];

            if (brandLineOutter.id === brandLineInner.id) {
                return false;
            }
        }
    }

    return true;
}

function getAllBrandLinesFromBrands(brands) {

    return brands.reduce(selectOnlyBrandLines, []);

    function selectOnlyBrandLines(previousBrandLines, currentBrand) {
        
        if (currentBrand.brandLines){
            currentBrand.brandLines.forEach(brandLine => previousBrandLines.push(brandLine));
        }

        return previousBrandLines;
    }
}

function getOnlyProductsFromBrandLines(brandLines) {

    return brandLines.reduce(selectOnlyProducts, []);

    function selectOnlyProducts(previousProducts, currentBrandLine) {

        if (currentBrandLine.products) {
            currentBrandLine.products.forEach(product => previousProducts.push(product));
        }

        return previousProducts;
    }
}