'use strict';

const should = require('should');

module.exports = function (app) {

    let server = {};

    describe('Collector', function () {

        before(function (done) {
            global.collector0 = require('../../server/collectors.json')['accounts'][0];
            // ensure that 'done' was called
            server = app.listen(function () {
                // login with collector0
                Request
                    .post('/api/Collectors/login')
                    .send(collector0)
                    .end(function (err, res) {
                        if (err) throw err;

                        // save admin token
                        collector0.token = res.body.id;
                        done();
                    });
            });
        });

        after(function (done) {
            server.close(done);
        });

        it('Should list all collectors accounts created via script.', shouldListAllCollectorsAccountsCreatedViaScript);
        it('GET /api/Products/productsWithPrice. Should list all products cross with retailers price.', shouldListAllProductsWithRetailersPrice);
        it('GET /api/Products/productsWithPrice?page=1&limit=3. Should list only 3 products of page 1.', shouldListOnly3ProductsOfPage1);
        it('PUT /api/Products/<id>/retailers. Should update price and availability of the product in a market.', shouldUpdatePriceAndAvailabilityOfProduct);
    });

    function shouldListAllCollectorsAccountsCreatedViaScript(andCallItWhenDone) {
        const Collector = app.models.Collector;
        Collector.find({}, function (err, collectors) {

            if (err) throw err;

            collectors.length.should.be.greaterThan(0);

            andCallItWhenDone();
        });
    }

    function shouldListAllProductsWithRetailersPrice(andCallItWhenDone) {
        Request
            .get('/api/Products/productsWithPrice')
            .end(function (errorOnRequest, response) {

                if (errorOnRequest) throw errorOnRequest;

                const productRetailer0 = response.body[0];

                productRetailer0.should.have.property('product');
                productRetailer0.product.brandLine.should.have.property('brand');
                productRetailer0.should.have.property('price');
                productRetailer0.should.have.property('available');

                andCallItWhenDone();
            });
    }

    function shouldListOnly3ProductsOfPage1(andCallItWhenDone) {
        Request
            .get('/api/Products/productsWithPrice?page=1&limit=3')
            .end(function (errorOnRequest, response) {

                if (errorOnRequest) throw errorOnRequest;

                response.body.length.should.be.equal(3);

                andCallItWhenDone();
            });
    }

    function shouldUpdatePriceAndAvailabilityOfProduct(andCallItWhenDone) {
        const newPrice = 9999999;
        const newAvailability = false;

        const productRetailerPriceAndAvailabilityChangeToHighAndFalse = {
            price: newPrice,
            available: newAvailability
        };

        Request
            .get('/api/Products?filter={"include":["retailers"]}')
            .end(function (errorOnRequestingProducts, response) {

                if (errorOnRequestingProducts) throw errorOnRequestingProducts;

                let products = response.body;

                const productId = products[0].id;
                const retailerId = products[0].retailers[0].id;

                const finalUrl = '/api/Products/' + productId + '/retailers/' + retailerId;

                Request
                    .put(finalUrl)
                    .send(productRetailerPriceAndAvailabilityChangeToHighAndFalse)
                    .set('Authorization', collector0.token)
                    .end(function (errorOnRequest, response) {

                        if (errorOnRequest) throw errorOnRequest;

                        const productRetailerRelation = response.body;
                        
                        productRetailerRelation.price.should.be.equal(newPrice);
                        productRetailerRelation.available.should.be.equal(newAvailability);

                        andCallItWhenDone();
                    })
            });

    }
};