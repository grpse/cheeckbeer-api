'use strict';

const should = require('should');

module.exports = function(app){

    let server = {};
    
    describe('Categories', forDescribe);

    function forDescribe(){
        before(function(done){
            // ensure that 'done' was called
            server = app.listen(done); 
        });

        after(function(done){
            server.close(done);
        });

        it ('GET /api/brandlines/. Should get all brand lines.', shouldGetAllBrandLines);
    }

    function shouldGetAllBrandLines(done){

        Request
            .get('/api/brandLines')
            .end(function(err, res){
                
                if(err) throw err;

                res.body.should.be.instanceOf(Array);
                const answer = res.body.filter(function(brandLine){
                    const ba = dev_temp_db.brandLines.filter(function(b){
                        return b.name === brandLine.name;
                    })[0];

                    return ba;
                });

                answer.length.should.be.equal(res.body.length);

                done();
            });
    }
}