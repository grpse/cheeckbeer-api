'use strict';
// Data base for development uses a file as database
// read dirs and require then to start testing
const fs = require('fs');
const path = require('path');
const MongoClient = require('mongodb').MongoClient;
const mongoURL = 'mongodb://localhost:27017/CheckBeerDev';
const async = require('async');

describe("Drop CheckBeerDev database with mongodb driver", function() {


    
    before(function(done) {

        this.timeout(10000);
        
        MongoClient.connect(mongoURL, runConnectionAndDropDatabase);
        function runConnectionAndDropDatabase(connectError, db) {

            if (connectError) done();
            else db.dropDatabase(function(dropError, result) {
                done();
            });
        }
    });

    it('Should run tests.', runTests);
});


function runTests(done) {

    this.timeout(10000);

    // used for creating
    global.dev_temp_db = require('./dev_temp_db');

    // Set development environment
    process.env.NODE_ENV = 'development';
    
    // run server
    const app = require('../server/server');
    const supertest = require('supertest');
    let serverStarted = false;
    const host = app.get('host');
    const port = app.get('port');
    const server_url = "http://" + host + ":" + port;
    // connect supertest server
    global.Request = supertest.agent(server_url);

    // start testing
    (function StartTesting() {
        const dirs = fs.readdirSync(__dirname);
        dirs.forEach(function (dir) {
            // is a dir?
            const stats = fs.lstatSync(path.resolve(__dirname, dir));
            if (stats.isDirectory())
                require('./' + dir)(app);
        });

        done()
    })();
}