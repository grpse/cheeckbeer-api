'use strict';

const should = require('should');
const async  = require('async');
const AUTHORIZATION = 'Authorization';


module.exports = function(app){

    // temporary storage for bars
    let bars = [];
    let barImages = [];
    let server = {}

    describe('Bars', function(){

        before(function(done){
            //get admin 0 credentials
            global.admin0 = Object.assign({}, require('../../server/admins.json')['accounts'][0]);

            
            // ensure that 'done' was called
            server = app.listen(function(){
                // login with admin0
                Request
                    .post('/api/Admins/login')
                    .send(admin0)
                    .end(function(err, res){
                        if(err) throw err;

                        // save admin token
                        admin0.token = res.body.id;
                        done();
                    });
            }); 
        });

        after(function(done){
            server.close(done);
        });


        it ('POST /api/bars/. Should create all bars that is described at temp database.', shouldCreateAllBars);
        it ('POST /api/bars/{id}/barImages. Should create bar images.', shouldCreatebarImages);        
        it ('POST /api/bars/{id}/barAds. Should create bar ads.', shouldCreateBarAds);
        it ('PUT /api/bars/{id}. Should update bars coordinate.', shouldUpdateBarCoordinate);
        it ('GET /api/bars/. Should get all bars.', shouldGetAllBars);
        it ('GET /api/bars/{id}/barImages. Should get all bar images.', shouldGetAllBarImages);
        it ('GET /api/bars/{id}/barAds. Should get all bar ads.', shouldGetAllBarAds);
    });

    function shouldCreateAllBars(done){
        // create all bars
        async.filterSeries(dev_temp_db.bars, function(barData, internalCallback){

            Request
                .post('/api/bars')
                .set('Authorization', admin0.token)
                .send(barData)
                .end(function(err, res){
                    if (err) throw err;

                    res.body.should.have.property('name');
                    res.body.should.have.property('address');

                    // push new bar
                    bars.push(res.body);

                   	internalCallback();
                });
        }, done);

    }

    function shouldCreatebarImages(done){

        async.filterSeries(dev_temp_db.barImages, function(barImage, internalCallback){

            // for each bar create this same bar image
            async.filterSeries(bars, function(bar, internal2Callback){
                Request
                    .post('/file/upload')
                    .attach('file', 'test/image.jpg')
                    .end(function(err, res){
                        if(err) throw err;

                        // save image id for this image of the bar
                        barImage.imageId = res.body.fileid;
                        // save bar id
                        barImage.barId = bar.id;

                        const barImageFinalUrl = '/api/bars/:id/barImages'.replace(':id', bar.id);                        

                        Request
                            .post(barImageFinalUrl)
                            .set('Authorization', admin0.token)
                            .send(barImage)
                            .end(function(err, res){
                                if(err) throw err;
                                
                                res.body.should.have.property('barId');
                                res.body.should.have.property('imageId');
                                
                                internal2Callback(null, res.body);
                            });
                    })

                    

            }, internalCallback);
        }, done);
    }

    function shouldCreateBarAds(done){
        async.filterSeries(dev_temp_db.barAds, function(barAd, internalCallback){

            // for each bar create this same bar image
            async.filterSeries(bars, function(bar, internal2Callback){
                Request
                    .post('/file/upload')
                    .attach('file', 'test/image.jpg')
                    .end(function(err, res){
                        if(err) throw err;

                        // save image id for this image of the bar
                        barAd.imageId = res.body.fileid;
                        // save bar id
                        barAd.barId = bar.id;

                        const barAdFinalUrl = '/api/bars/:id/barAds'.replace(':id', bar.id);                        

                        Request
                            .post(barAdFinalUrl)
                            .set('Authorization', admin0.token)
                            .send(barAd)
                            .end(function(err, res){
                                if(err) throw err;
                                
                                res.body.should.have.property('barId');
                                res.body.should.have.property('imageId');
                                
                                internal2Callback(err, res.body);
                            });
                    });
            }, internalCallback);
        }, done);
    }

    function shouldUpdateBarCoordinate(done) {

        const barId = bars[0].id;
        const finalBarURL = '/api/bars/:id/'.replace(':id',barId); 
        const newCoordinateData = {
            location : {
                lat : -29.920769,
                lng : -51.071144
            }
        };

        Request
            .put(finalBarURL)
            .send(newCoordinateData)
            .set(AUTHORIZATION, admin0.token)
            .end(handleBarCoordinateUpdate);

        function handleBarCoordinateUpdate(errorOnRequest, res) {
            
            if (errorOnRequest) throw errorOnRequest;
            const barUpdated = res.body;
            
            const latReceived = barUpdated.location.lat.toString();
            const lngReceived = barUpdated.location.lng.toString();
            const latWish = newCoordinateData.location.lat.toString();
            const lngWish = newCoordinateData.location.lng.toString();
            
            latReceived.should.be.equal(latWish);
            lngReceived.should.be.equal(lngWish);

            done();
                       
        }
    }

    function shouldGetAllBars(done){
        
        // try get bar info by id
        Request
            .get('/api/bars/')
            .end(function(err, res){
                if(err) throw err;

                res.body.should.instanceOf(Array);
                res.body.length.should.greaterThan(0);

                done();
            });
    }

    function shouldGetAllBarImages(done){

        // for all bars, try to get bar images
        async.filterSeries(bars, function(bar, internalCallback){

            const barImageFinalUrl = '/api/bars/:id/barImages'.replace(':id', bar.id);

            Request
                .get(barImageFinalUrl)
                .end(function(err, res){

                    if(err) throw err;
                    
                    res.body.should.instanceOf(Array);
                    res.body.length.should.greaterThan(0);

                    internalCallback(null, true);
                });

        },done);
    }

    function shouldGetAllBarAds(done){
        // for all bars, try to get bar ads
        async.filterSeries(bars, function(bar, internalCallback){

            const barAdFinalUrl = '/api/bars/:id/barAds'.replace(':id', bar.id);

            Request
                .get(barAdFinalUrl)
                .end(function(err, res){

                    if(err) throw err;
                    
                    res.body.should.instanceOf(Array);
                    res.body.length.should.greaterThan(0);

                    internalCallback(null, true);
                });

        },done);
    }
};